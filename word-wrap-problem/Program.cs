﻿using System;
using System.Collections.Generic;
using System.IO;

namespace word_wrap_problem
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Please Paste In The Path Of the File you will like to wrap and press enter");

                Console.WriteLine(@"---e.g C:\WordWrapFolder\testfile.txt---");

                var filePath = Console.ReadLine();
                
                Console.WriteLine("Enter your Line Length (Make Sure it is a number) and press enter:");

                var length = Convert.ToInt32(Console.ReadLine());

                var wordsToProcess = ReadFromFile(@filePath);

                foreach (var word in wordsToProcess)
                {
                    var wrappedWords = Wrapwords(word, length);

                    WriteToFile(wrappedWords);
                }

                Console.WriteLine("");

                Console.WriteLine("Please Open the file named 'WrappedWords.txt' in the root folder to see the results");

                Console.ReadLine();
            }
            catch (Exception ex) 
            {
                Console.WriteLine(ex.Message);

                Console.WriteLine("Please Close The Application and input the correct details.");

                Console.ReadLine();
            }
            

        }

        private static List<string> Wrapwords(string sentence, int length)
        {
            List<string> subSentences = new List<string>();

            string tempSentence = string.Empty;

            int lengthCounter = 0;

            if ((length < 1) && (sentence.Length > 1))
            {
                subSentences.Add(sentence);

                return subSentences;
            }
            else if (sentence.Length <= length)
            {
                subSentences.Add(sentence);

                return subSentences;
            }
            else
            {
                char[] ch = new char[sentence.Length];

                for (int i = 0; i < sentence.Length; i++)
                {
                    ch[i] = sentence[i];
                }

                while ((sentence.Length - lengthCounter) > length)
                {
                    for (int j = 0; j < ch.Length; j++)
                    {
                        if (tempSentence.Length < length)
                        {
                            tempSentence = $"{tempSentence}{ch[j]}";
                        }
                        else
                        {
                            subSentences.Add(tempSentence);

                            lengthCounter = lengthCounter + tempSentence.Length;

                            tempSentence = string.Empty;

                            tempSentence = $"{tempSentence}{ch[j]}";
                        }

                    }

                }

                string lastSubSentence = sentence.Substring(lengthCounter);

                subSentences.Add(lastSubSentence);

                return subSentences;

            }
        }

        private static List<string> ReadFromFile(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                throw new Exception("Please Pass in the file Path");
            }
            else
            {
                string[] fileToProcess = File.ReadAllLines(filePath);

                List<string> sentenceList = new List<string>(fileToProcess);

                return sentenceList;
            }
        }

        private static void WriteToFile(List<string> words)
        {
            using var textWriter = new StreamWriter("WrappedWords.txt", true);

            foreach (var w in words) 
            {
                textWriter.WriteLine(w);
            }
               
        }
    }

            
            
            
        
    
}
